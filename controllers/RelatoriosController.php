<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;

 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
 
   public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT nome,  mc_respostasalunos.id, mc_respostasalunos.respostas
        FROM mc_aluno JOIN mc_respostasalunos ON mc_aluno.id = mc_respostasalunos.aluno
        ORDER BY nome, mc_respostasalunos.id ASC',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }
   public function actionRelatorio2()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT mc_aluno.nome as aluno, mc_turma.nome as turma
        FROM mc_aluno JOIN mc_turma ON mc_aluno.turma= mc_turma.id
        ORDER BY mc_aluno.nome, mc_turma.nome ASC',
            ]
        );
        
        return $this->render('relatorio2', ['resultado' => $consulta]);
   }
   public function actionRelatorio3()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT mc_turma.nome as turma, COUNT(mc_aluno.nome) as quantidade FROM mc_aluno
        JOIN mc_turma ON mc_aluno.turma= mc_turma.id
        GROUP BY mc_turma.nome
        ORDER BY COUNT(mc_aluno.nome) ASC',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $consulta]);
   }
   public function actionRelatorio4()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT COUNT(mc_turma.nome) as Quantidade FROM mc_turma',
            ]
        );
        
        return $this->render('relatorio4', ['resultado' => $consulta]);
   }
   public function actionRelatorio5()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT mc_aluno.nome FROM mc_aluno',
            ]
        );
        
        return $this->render('relatorio5', ['resultado' => $consulta]);
   }

}

