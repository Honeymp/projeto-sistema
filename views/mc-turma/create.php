<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\McTurma */

$this->title = 'Adicionar Turma';
$this->params['breadcrumbs'][] = ['label' => 'Turmas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mc-turma-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
