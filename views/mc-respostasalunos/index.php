<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\McRespostasalunosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gabaritos Alunos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mc-respostasalunos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Resposta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'respostas',
            'aluno',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
