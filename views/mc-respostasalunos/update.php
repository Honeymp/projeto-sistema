<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\McRespostasalunos */

$this->title = 'Editar Respostas alunos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Respostas Alunos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="mc-respostasalunos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
