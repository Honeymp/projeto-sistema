<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\McRespostasalunos */

$this->title = 'Gabaritos alunos';
$this->params['breadcrumbs'][] = ['label' => 'Gabaritos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mc-respostasalunos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
