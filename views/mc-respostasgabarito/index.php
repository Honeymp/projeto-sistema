<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\McRespostasgabaritoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Respostas Gabaritos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mc-respostasgabarito-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar Gabarito', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'respostas',
            'turma',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
