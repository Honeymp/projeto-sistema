<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\McRespostasgabarito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mc-respostasgabarito-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'respostas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'turma')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
