<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\McRespostasgabarito */

$this->title = 'Editar Respostas gabarito: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Respostas gabaritos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="mc-respostasgabarito-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
