<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Sobre';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Sistema criado para cadastrar gabaritos referente aos Simulados aplicados
        no segundo e terceiro trimestre no IFNMG.
    </p>

    
</div>
