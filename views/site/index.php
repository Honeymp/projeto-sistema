<?php

/* @var $this yii\web\View */

$this->title = 'Simulado';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bem vindo!</h1>

        <p class="lead">O que você deseja fazer?</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2>Turmas</h2>

                <p>escrever algo.</p>

                <p><a class="btn btn-default" href="http://localhost/projeto-sistema/web/index.php?r=mc-turma%2Findex">Ver &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Gabaritos</h2>

                <p>escrever algo.</p>
                
                <p><a class="btn btn-default" href="http://localhost/projeto-sistema/web/index.php?r=mc-respostasgabarito%2Findex">Ver Respostas &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Respostas Alunos</h2>

                <p>escrever algo.</p>

                <p><a class="btn btn-default" href="http://localhost/projeto-sistema/web/index.php?r=mc-respostasalunos%2Findex">Ver Respostas Alunos &raquo;</a></p>
            </div>
            <div class="col-lg-3">
                <h2>Alunos</h2>

                <p>Ir para página.</p>

                <p><a class="btn btn-default" href="http://localhost/projeto-sistema/web/index.php?r=mc-aluno%2Findex">Ver Alunos &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
