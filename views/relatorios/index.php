<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
 
</div>
<?= Html::a('Respostas do aluno', ['relatorio1'], ['class' => 'btn btn-success']) . "<br> <br>"?>
<?= Html::a('Alunos e sua respectiva turma', ['relatorio2'], ['class' => 'btn btn-success']) . "<br> <br>"?>
<?= Html::a('Quantidade de aluno por turma', ['relatorio3'], ['class' => 'btn btn-success']) . "<br> <br>"?>
<?= Html::a('Quantidade de turmas existentes', ['relatorio4'], ['class' => 'btn btn-success']) . "<br> <br>"?>
<?= Html::a('Alunos cadastrados', ['relatorio5'], ['class' => 'btn btn-success']) . "<br> <br>"?>
