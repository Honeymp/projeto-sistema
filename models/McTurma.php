<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_turma".
 *
 * @property int $id
 * @property string $nome
 *
 * @property McAluno[] $mcAlunos
 * @property McRespostasgabarito[] $mcRespostasgabaritos
 */
class McTurma extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mc_turma';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcAlunos()
    {
        return $this->hasMany(McAluno::className(), ['turma' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcRespostasgabaritos()
    {
        return $this->hasMany(McRespostasgabarito::className(), ['turma' => 'id']);
    }
}
