<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_respostasgabarito".
 *
 * @property int $id
 * @property string $respostas
 * @property int $turma
 *
 * @property McTurma $turma0
 */
class McRespostasgabarito extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mc_respostasgabarito';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turma'], 'integer'],
            [['respostas'], 'string', 'max' => 1],
            [['turma'], 'exist', 'skipOnError' => true, 'targetClass' => McTurma::className(), 'targetAttribute' => ['turma' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respostas' => 'Respostas',
            'turma' => 'Turma',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma0()
    {
        return $this->hasOne(McTurma::className(), ['id' => 'turma']);
    }
}
