<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_respostasalunos".
 *
 * @property int $id
 * @property string $respostas
 * @property int $aluno
 *
 * @property McAluno $aluno0
 */
class McRespostasalunos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mc_respostasalunos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'aluno'], 'required'],
            [['id', 'aluno'], 'integer'],
            [['respostas'], 'string', 'max' => 1],
            [['id', 'aluno'], 'unique', 'targetAttribute' => ['id', 'aluno']],
            [['aluno'], 'exist', 'skipOnError' => true, 'targetClass' => McAluno::className(), 'targetAttribute' => ['aluno' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'respostas' => 'Respostas',
            'aluno' => 'Aluno',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAluno0()
    {
        return $this->hasOne(McAluno::className(), ['id' => 'aluno']);
    }
}
