<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mc_aluno".
 *
 * @property int $id
 * @property string $nome
 * @property int $turma
 *
 * @property McTurma $turma0
 * @property McRespostasalunos[] $mcRespostasalunos
 */
class McAluno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mc_aluno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turma'], 'integer'],
            [['nome'], 'string', 'max' => 100],
            [['turma'], 'exist', 'skipOnError' => true, 'targetClass' => McTurma::className(), 'targetAttribute' => ['turma' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'turma' => 'Turma',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma0()
    {
        return $this->hasOne(McTurma::className(), ['id' => 'turma']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMcRespostasalunos()
    {
        return $this->hasMany(McRespostasalunos::className(), ['aluno' => 'id']);
    }
}
