CREATE DATABASE trabalhofinal;
USE trabalhofinal;

CREATE TABLE mc_aluno(
id INT AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(100),
turma int,
FOREIGN KEY (turma) REFERENCES mc_turma(id)
);

CREATE TABLE mc_turma(
id INT AUTO_INCREMENT PRIMARY KEY,
nome VARCHAR(20)
);

CREATE TABLE mc_respostasalunos(
id INT AUTO_INCREMENT PRIMARY KEY,
respostas CHAR(1),
aluno int,
FOREIGN KEY (aluno) REFERENCES mc_aluno(id)
);

CREATE TABLE mc_respostasgabarito(
id INT AUTO_INCREMENT PRIMARY KEY,
respostas CHAR(1),
turma int,
FOREIGN KEY (turma) REFERENCES mc_turma(id)
);